const int ledPin = 13;

void enter(){
  Keyboard.set_key1(KEY_ENTER);
  Keyboard.send_now();
  Keyboard.set_key1(0);
  Keyboard.send_now();
  
}

 void clearKeys (){
      delay(200);
      Keyboard.set_key1(0); 
      Keyboard.set_key2(0);
      Keyboard.set_key3(0);
      Keyboard.set_key4(0);
      Keyboard.set_key5(0);
      Keyboard.set_key6(0);
      Keyboard.set_modifier(0);
      Keyboard.send_now(); 
  }
  
    void sendKey(uint8_t keyin){
      Keyboard.set_key1(keyin);
      Keyboard.send_now();
      clearKeys();
      delay(200);
  }
  
  
void setup() {
  pinMode(ledPin, OUTPUT);
  delay(2000);
  Keyboard.set_modifier(MODIFIERKEY_GUI);
  Keyboard.send_now();
  Keyboard.set_key1(KEY_R);
  Keyboard.send_now();
  clearKeys();
  delay(200);
  Keyboard.println("cmd.exe");
  delay(200);
  Keyboard.println("ipconfig /all");
  Keyboard.println("ping -t 8.8.8.8");
  delay(10000);
  Keyboard.set_modifier(MODIFIERKEY_CTRL);
  Keyboard.send_now();
  Keyboard.set_key1(KEY_C);
  Keyboard.send_now();
  clearKeys();  


}

void loop() {
  digitalWrite(ledPin, HIGH);   // set the LED on
  delay(1000);                  // wait for a second
  digitalWrite(ledPin, LOW);    // set the LED off
  delay(1000);                  // wait for a second
}

