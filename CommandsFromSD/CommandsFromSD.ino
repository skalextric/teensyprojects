#include <SD.h>
#include <SPI.h>

File myFile;


// Teensy 3.1: pin 10

const int chipSelect = 10;
const int ledPin = 13;

void enter() {
  Keyboard.set_key1(KEY_ENTER);
  Keyboard.send_now();
  Keyboard.set_key1(0);
  Keyboard.send_now();

}

void clearKeys () {
  delay(200);
  Keyboard.set_key1(0);
  Keyboard.set_key2(0);
  Keyboard.set_key3(0);
  Keyboard.set_key4(0);
  Keyboard.set_key5(0);
  Keyboard.set_key6(0);
  Keyboard.set_modifier(0);
  Keyboard.send_now();
}

void sendKey(uint8_t keyin) {
  Keyboard.set_key1(keyin);
  Keyboard.send_now();
  clearKeys();
  delay(200);
}



int readline(int readch, char *buffer, int len)
{
  static int pos = 0;
  int rpos;

  if (readch > 0) {
    switch (readch) {
      case '\n': // Ignore new-lines
        break;
      case '\r': // Return on CR
        buffer[pos++] = '\0';
        rpos = pos;
        pos = 0;  // Reset position index ready for next time
        return rpos;
      default:
        if (pos < len-1) {
          buffer[pos++] = readch;
          buffer[pos] = 0;
        }
    }
  }
  // No end of line has been found, so return -1.
  return -1;
}

void setup()
{

  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE1);

  // It is VERY IMPORTANT that the pins be reassigned AFTER ALL
  // SPI initialization INCLUDING datamode or bitorder etc.

  // First reassign pin 13 to Alt1 so that it is not SCK but the LED still works
  CORE_PIN13_CONFIG = PORT_PCR_MUX(0);
  // and then reassign pin 14 to SCK
  CORE_PIN14_CONFIG = PORT_PCR_DSE | PORT_PCR_MUX(2);

  // Open serial communications and wait for port to open:
  Serial.begin(9600);

  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  

  Serial.print("Initializing SD card...");

  pinMode(10, OUTPUT);

  if (!SD.begin(chipSelect)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  // remove and open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  Serial.println("Borrando...");
  if (SD.remove("test.txt"))
    Serial.println("Completo");
  myFile = SD.open("test.txt", FILE_WRITE);



  // if the file opened okay, write to it:
  if (myFile) {
    Serial.println("Commands to execute:");
    static char buff[80];
    while (true){
    while(true){
       if(readline(Serial.read(), buff, 80) != -1) 
         break;
    }
    myFile.println(buff);
    if (strcmp(buff, "cont")==0) break;

    }
    myFile.close();
    Serial.println("Executing....");
  }
  else {

    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  
  //OpenCMD

  delay(2000);
  Keyboard.set_modifier(MODIFIERKEY_GUI);
  Keyboard.send_now();
  Keyboard.set_key1(KEY_R);
  Keyboard.send_now();
  clearKeys();
  delay(200);
  Keyboard.println("cmd.exe");
  delay(200);
  

  // re-open the file for reading:
  myFile = SD.open("test.txt");
  
  if (myFile) {
    Serial.println("test.txt:");
    while(true){
    // read from the file until there's nothing else in it:

    char val[32];

    int i = 0;

    while (val[i - 1] != '\r') {
      if (val[i - 1] == '\n') i--;
      val[i] = myFile.read();
      i++;
    }
    val[i-1] = '\0';    
    if (strcmp(val, "cont")==0) break;
    Serial.print(val);
    Keyboard.println(val);
    }
    delay(5000);
    Keyboard.set_modifier(MODIFIERKEY_CTRL);
    Keyboard.send_now();
    Keyboard.set_key1(KEY_C);
    Keyboard.send_now();
    clearKeys();
    Keyboard.println("exit");


    // close the file:
    myFile.close();
  }
  else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }

  Serial.end();
}

void loop()
{
  digitalWrite(ledPin, HIGH);
  delay(500);
  digitalWrite(ledPin, LOW);
  delay(500);
}






